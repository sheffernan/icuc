var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var redis = require('redis');
var store = redis.createClient();
var room = "default";

/* Bind redis error handler to log */
store.on('error', function(err) {
    console.log('Redis Error: ' + err);
});

/* Some basic routes */
app.get('/', function(req, res) {
    //res.send("<h1>Hello World</h1>");
    res.sendfile('index.html');
});

io.on('connection', function(socket){
    // Add our vote slot to the hash
    store.hset(room, socket.id, null);
    console.log(socket.id + ': a user has connected');
    store.hlen(room, function(err, res) {
        console.log('participants: ' + res);
        io.emit('participant count', res);
    });
    //socket.broadcast.emit('chat message', 'Someone connected.');

    socket.on('reveal please', function(msg) {
        console.log('reveal: ' + msg);
        //io.emit('chat message', msg);
        //socket.broadcast.emit('chat message', msg);
        store.hgetall(room, function(err, obj) {
            io.emit('tally', obj);
            console.dir(obj);
        });
    });

    socket.on('vote', function(msg) {
        // key/field/value
        store.hset(room, socket.id, msg);
        console.log(socket.id + ': vote: ' + msg);
        // let others know more votes are in
        store.hgetall(room, function(err, obj) {
            var voted = 0;
            console.log(obj);
            for( key in obj ) {
                if( !obj.hasOwnProperty(key) ) {
                    console.log('vote uncounted: ' + key);
                    continue;
                }
                if( obj[key] != 'null' ) {
                    voted++;
                }
                    console.log('vote counted: ' + key + '; votes: ' + voted);
            }
            io.emit('voted', voted); 
        });
    });

/*    socket.on('reveal please', function() {
        console.log(store.hgetall(room));
        socket.broadcast.emit('tally', '');
    });*/

    socket.on('reload please', function() {
        // Delete the room, and trigger every client to reload
        store.del(room, function(err, obj) {
            io.emit('reload');
        });
    });

    socket.on('reset please', function() {
        io.emit('reset');
        store.hgetall(room, function(err, obj) {
            // Reset everyone's votes
            for( key in obj ) {
                if( !obj.hasOwnProperty(key) ) {
                    continue;
                }
                obj[key] = null;
            }
            store.hmset(room, obj);
        });
    });

    socket.on('disconnect', function() {
        console.log(socket.id + 'user disconnected');
        // Remove our vote slot from the hash
        store.hdel(room,socket.id);
        // Emit the number of participants
        store.hlen(room, function(err, res) {
            console.log('participants: ' + res);
            io.emit('participant count', res);
        });
    });
});

http.listen(3000, function() {
    console.log('listening on *:3000');
});


